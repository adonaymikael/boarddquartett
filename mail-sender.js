const mailer = require('nodemailer');


let authenticatedUser;

if(process.env.EMAIL_ADDRESS != null && process.env.EMAIL_PASS != null){
    authenticatedUser = {
        email: process.env.EMAIL_ADDRESS,
        senha: process.env.EMAIL_PASS
    }
}else{
    authenticatedUser = {
            email: 'projetouniceub2017@gmail.com',
            senha: 'uniceub2112'
    }
}

let dados = {
    service: "gmail",
    port: 465,
    auth: {
        user: authenticatedUser.email,
        pass: authenticatedUser.senha
    }
}

const transporter = mailer.createTransport(dados);

console.log(dados)

let mailSender = {
  send: (data) => {
    
    let mailOptions = {
      from: authenticatedUser.email,
      to: authenticatedUser.email,
      subject: "Contato - BoarddQuartett Website",
      text: returnBody(data) 
    };

    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log('Erro ao enviar e-mail:  ' + error);
        }else{
            console.log('Email enviado com sucesso! ' + info.response);
        }
    });       
    }
}

function returnBody(form_data){
  let message_body = `Uma mensagem foi enviada atráves do website...\nSegue informações: \n\nNome: ${form_data.nome}\nTelefone: ${form_data.telefone}\nEmail: ${form_data.email}\nMensagem: ${form_data.mensagem}\n\n`
  return message_body;
}

module.exports = mailSender;

