//Install express server
const express = require('express');
const path = require('path');
const mail_sender = require('./mail-sender');
const app = express();
const porta = process.env.PORT || 8080;

app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
 
// Serve only the static files form the dist directory
// Replace the '/dist/Boardd-Quartett'
app.use(express.static(__dirname + '/dist/Boardd-Quartett'));
 
app.get('*', function(req,res) {
  // Replace the '/dist/Boardd-Quartett/index.html'
  res.sendFile(path.join(__dirname + '/dist/Boardd-Quartett/index.html'));
});

app.post('/api/email', function(req,res) {
  mail_sender.send(req.body);
  res.send("success");
});

// Start the app by listening on the default Heroku port
app.listen(porta, () => {
  console.log('Servidor rodando...\nPorta:', porta);
});