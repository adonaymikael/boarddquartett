import { Component, OnInit } from '@angular/core';
import { EmailRequestService } from './../services/email-request.service';


@Component({
  selector: 'app-page-contato',
  templateUrl: './page-contato.component.html',
  styleUrls: ['./page-contato.component.css']
})
export class PageContatoComponent implements OnInit {

  nome: string;
  telefone: string;
  email: string;
  mensagem: string;

  constructor(private emailRequest: EmailRequestService) { }

  ngOnInit() {
  }

  sendNotification() {
    let values: Object;
    if (this.email.length < 10) {
      alert("Favor inserir um e-mail válido!");
    }

    if (this.telefone.length < 8) {
      alert("Favor inserir um número de telefone válido!");
    }

    if (this.nome.length > 3 && this.telefone.length >= 8 && this.email.length > 10 && this.mensagem.length > 1) {

      values = {
        'nome': this.nome,
        'telefone': this.telefone,
        'mensagem': this.mensagem,
        'email': this.email
      };

      this.emailRequest.sendEmail(values);
    }
  }
}
