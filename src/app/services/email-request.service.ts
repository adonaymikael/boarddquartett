import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class EmailRequestService {
  
  private REST_API_SERVER = `${window.location.port}/api/email`;

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json', }), responseType: 'text' as 'json' };

  constructor(private httpClient: HttpClient) { }

  public sendEmail(data: Object){
    console.log('Enviando e-mail...');
    this.httpClient.post(this.REST_API_SERVER, data, this.httpOptions).subscribe(
      response => {
        alert("E-mail enviado com sucesso!")
        console.log(response);
          
      },
    val => {
      alert("Não foi possivel enviar o E-mail.\nTente novamente.")
    });
  }
  
}
