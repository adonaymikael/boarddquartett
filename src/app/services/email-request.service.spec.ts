import { TestBed } from '@angular/core/testing';

import { EmailRequestService } from './email-request.service';

describe('EmailRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmailRequestService = TestBed.get(EmailRequestService);
    expect(service).toBeTruthy();
  });
});
