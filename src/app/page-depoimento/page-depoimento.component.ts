import { Component, OnInit } from '@angular/core';
import {ConstantesUtil} from '../../util/contantes.util';

@Component({
  selector: 'app-page-depoimento',
  templateUrl: './page-depoimento.component.html',
  styleUrls: ['./page-depoimento.component.css']
})
export class PageDepoimentoComponent implements OnInit {
  popUp: boolean;
  depoimentos = ConstantesUtil.DEPOIMENTOS;
  depoimentoSelecionado: any;
  limit = 2;

  constructor() { }

  ngOnInit() {
  }

  openView(dep: any) {
    this.popUp = true;
    this.depoimentoSelecionado = dep;
  }

  moreDepoiments() {
    this.limit == 2 ? this.limit = this.depoimentos.length : this.limit = 2;
  }
}
