import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import { Component, OnInit } from '@angular/core';
import { PageEquipeModalComponent } from './page-equipe-modal/page-equipe-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-page-equipe',
  templateUrl: './page-equipe.component.html',
  styleUrls: ['./page-equipe.component.css'],
})
export class PageEquipeComponent implements OnInit {
  popUp: boolean;

  constructor(public dialog: MatDialog){}

  profissional = [
    {nome: "Cléia Leimueller", img: '././assets/Cleia Leimueller.png',
     text: 'Pós Graduação em Metodologia do Ensino Superior <br>UNEB – Universidade Estadual da Bahia.'
     +'<br>Graduação Licenciatura em Ciências Naturais (Matemática, Física, Química e Biologia) UFBA - Universidade Federal da Bahia.'
     +'<br>Graduação em Biomedicina – PUC – Universidade Católica de Santa Catarina.'
     +'<br><br>&#8226 Consciência Sistêmica.'
     +'<br>&#8226 Bar Access Consciouness Practioner.'
     +'<br>&#8226 Terapia Consciência Quantica.'
     +'<br>&#8226 Reike Xamânico.'
     +'<br>&#8226 Orixá Reike.'
     +'<br>&#8226 Patologia.'
     +'<br>&#8226 Análises Clínicas.'
     +'<br>&#8226 Estética.'
     +'<br>&#8226 Mestre em Reike pela Universidade Humaniversidade.'
     +'<br>&#8226 Vivência e Convivências Etno-Biologia/Antropologia/Cultura Religiosa – Universidade Federal da Bahia.'
     +'<br>&#8226 Bioenergética – Alemanha / Áustria / Portugal.'
     +'<br>&#8226 Imersão em Radiologia – Ênfase em Ressonância Magnética – Albert Einstein/SP.'
     +'<br>&#8226 Mesoterapia Homeopática – Portugal.'
     +'<br>&#8226 Mesoterapia Estética – São Paulo & Portugal.'
     +'<br>&#8226 Embriologia – PUC – Universidade Católica de Santa Catarina.'
     +'<br><br>Experiência profissional:'
     +'<br>Bioenergética (10anos) Patologia.'
     +'<br>Análises Clínicas.'
     +'<br>Estética.'
     +'<br>Reikiana (09anos).' },

    {nome: "Ana Di Goya", img: '././assets/Ana Di Goya.png',
    text: '&#8226; Larga experiência como revisora de util tanto na área Corporativa em Departamentos de Comunicações como no meio acadêmico na revisão e adequação de artigos científicos, monografias e teses de mestrado e doutorado.'
    + '<br>&#8226; Analista Corporal e Comportamental.'
    + '<br>&#8226; Estudante de Letras,Teosofia e Consciência Sistêmica' },

    {nome: "Ligia Correia", img: '././assets/Ligia Correia.png',
    text: '2017 - Pós graduada em Gerenciamento Ambiental - ESALQ/USP'
    +'<br>2005 - Pós graduada em Administração Sistêmica - NAIPPE/USP'
    +'<br>2005 - Técnica Plena em Seguros - FENACOR'
    +'<br>1989 - Licenciatura - Letras - Inglês/Portugês - FMU'
    +'<br>1983 - Técnica em Secretariado - RADIAL'
    +'<br><br>32 anos de atuação no ramo corporativo relacionando-se com diversos níveis hierárquicos.'
    +'<br>05 anos sócia-proprierária Correia & Bastos Seguros'
    +'<br>05 anos sócia-proprietária corretora de seguros todos os ramos'
    +'<br>12 anos como voluntária Congregação Luterana e Vila Joanisa'
    +'<br><br>&#8226 Consciência Sistêmica'
    +'<br>&#8226 Coach, PNL'
    +'<br>&#8226 Constelação Sistêmica '
    +'<br>&#8226 Bar Access Consciouness Practioner'
    +'<br>&#8226 René Mey - Regeneração Molecular, Expressão Celular '
    +'<br>&#8226 Reik Chama Violeta'
    +'<br>&#8226 TQC - Transformação Quântica da Consciência'
    +'<br>&#8226 Cursos Seara Bendita - em andamento' },

    {nome: "Eliane Marra", img: '././assets/Eliane Marra.png',
     text: '&#8226 Graduada em Fisioterapia pela Unecerp'
     +'<br>&#8226 Consteladora Sistêmica'
     +'<br>&#8226 Consciência Sistêmica'
     +'<br>&#8226 Analista Corporal e Comportamental'
     +'<br>&#8226 14 anos de experiência em Fisioterapia Clínica, Ortopédica e RPG.' }

  ];
  profissionalSelecionado: any;

  equipeModal(dep: any){
    var profissional = new Object();
    this.popUp = true;
    this.profissionalSelecionado = dep;

    let dialogRef = this.dialog.open(PageEquipeModalComponent, {
      panelClass: 'custom-dialog-container',
      height: '800px',
      width: '1000px',
      data: dep,

    });
  }

  ngOnInit() {
  }

}
