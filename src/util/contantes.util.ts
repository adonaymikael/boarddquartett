export class ConstantesUtil{

  constructor(){}

  public static DEPOIMENTOS=[
    {nome: "Suzi Pena",
      img: '././assets/people1.png',
      descricao : "Sou grata a todo aprendizado que obtive durante o curso que fiz do tabuleiro, ele me agregou muito, facilitou meus atendimento, gratidão ao quarteto, vocês me abriram portas, janelas, tive muitos insights, e estou colocando em prática. Gratidão! E que vocês continuem passando esse conhecimento! Gratidão a todas do grupo também, com cada uma eu aprendi um pouquinho! Um beijo no coração de cada uma! ", rating: 5 },

    {nome: "Maria Sahab Estrela",
      img: '././assets/people2.png',
      descricao : "Gratidão imensa pela partilha, super contribuição esse curso do Tabuleiro, e essa proposta de integração do ser é show! Equilíbrio, mente, corpo e emoções é o caminho! Parabéns ao quarteto de professoras! Valeu! Grupo lindo, de grande contribuição, mas estou com um celular com pouca memória, estou saindo de alguns grupos por enquanto, até a troca do aparelho que já está em andamento com facilidade e alegria! Bora olhar com carinho para nosso núcleo mais profundo integrando tudo que somos com facilidade e alegria! Mulheres Super Power! Gratidão!", rating: 5 },

    {nome: "Dilma Fernandes Silva",
      img: '././assets/people3.png',
      descricao : "Gratidão! Com validade para todo o sempre! Pela sessão de ontem e por todos os ensinamentos e carinho transmitidos por esse Quarteto maravilhoso! Espero continuar fazendo parte dessa corrente de amor.", rating: 5 },

    {nome: "Mônica Henriques de Oliveira",
      img: '././assets/people4.png',
      descricao : "Gratidão ao quarteto. Vocês são especiais, cada uma com sua essência e encanto. Me sinto honrada.", rating: 5 },
  ];
}
