## Link para acessar o projeto.
[BoarddQuartett.herokuapp](https://boarddquartett.herokuapp.com/home)

### Imagem do projeto atual:
![](/prints/img1.JPG)

# **Instruções de linha de comando Git:**

### **Configuração global do Git**
```
git config --global user.name "username"
git config --global user.email "usermail"
```

### **Repositório Git existente**
```
git clone https://gitlab.com/adonaymikael/boarddquartett.git
cd boarddquartett
git add .
git commit -m "add folder"
git push -u origin master
```
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

